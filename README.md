# SequenceIO

Template python project for test-driven development

## Contributors

| Name                   | email                                 |
|------------------------|---------------------------------------|
| Carsten Fortmann-Grote | carsten.fortmann-grote@evolbio.mpg.de |
| Neel Prabh             | prabh@evolbio.mpg.de                  |
| Wagner Fagundes        | fagundes@evolbio.mpg.de               | 
